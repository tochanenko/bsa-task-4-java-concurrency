create function count_bits(bit_string bit(64)) returns integer
as
$$
select length(replace(x::text, '0', ''))
from (values (bit_string::char varying)) as something(x);
$$
language SQL;