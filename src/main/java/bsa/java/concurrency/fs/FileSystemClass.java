package bsa.java.concurrency.fs;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;

@Component(value = "fileSystem")
public class FileSystemClass implements FileSystem {
    private static final Logger logger = LoggerFactory.getLogger(FileSystemClass.class);

    @Value("${images.upload-directory}")
    private String uploadDirectory;

    @Override
    public CompletableFuture<String> saveFile(String path, byte[] file, URI uri) {
        try {
            Path pathToFile = Paths.get(uploadDirectory + "/" + path + ".jpg");
            Path parentPath = pathToFile.getParent();
            if (!Files.exists(parentPath)) {
                Files.createDirectories(parentPath);
            }
            Files.write(pathToFile.toAbsolutePath(), file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return CompletableFuture.completedFuture(ServletUriComponentsBuilder.fromUri(uri)
        .path(uploadDirectory)
        .path(path)
        .toUriString());
    }

    @Override
    public CompletableFuture<Void> deleteByPath(String path) {
        Path filePath = Paths.get(uploadDirectory + "/" + path + ".jpg");
        if (Files.exists(filePath)) {
            return CompletableFuture.runAsync(() -> {
                System.out.println(new File(filePath.toString()).delete());
//                try {
//                    FileUtils.forceDelete(new File(filePath.toString()));
//                } catch (IOException e) {
//                    logger.error(e.getMessage());
//                }
            });
        }

        return CompletableFuture.completedFuture(null);
    }

    @Override
    public CompletableFuture<Void> deleteAll() {
        Path filePath = Paths.get(uploadDirectory);

        if (Files.exists(filePath)) {
            try {
                FileUtils.cleanDirectory(new File(filePath.toString()));
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }

        return CompletableFuture.completedFuture(null);
    }
}
