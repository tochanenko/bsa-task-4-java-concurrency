package bsa.java.concurrency.utils.mappers;

import bsa.java.concurrency.image.ImageEntity;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.utils.hash.DiagonalHash;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ImageMapper {
    @Qualifier("diagonalHasher")
    private final DiagonalHash diagonalHash;

    public SearchResultDTO entityToDto(ImageEntity imageEntity, long hash) {
        return SearchResultDTO.builder()
                .imageId(imageEntity.getUuid())
                .matchPercent(diagonalHash.calculateThreshold(hash, imageEntity.getHash()))
                .imageUrl(imageEntity.getPath())
                .build();
    }

    public List<SearchResultDTO> entityListToDtoList(List<ImageEntity> imageEntities, long hash) {
        return imageEntities.stream().map(t -> entityToDto(t, hash)).collect(Collectors.toList());
    }
}
