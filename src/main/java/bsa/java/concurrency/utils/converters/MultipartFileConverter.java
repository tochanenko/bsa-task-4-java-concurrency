package bsa.java.concurrency.utils.converters;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class MultipartFileConverter {
    public List<byte[]> fileArrayToBytes(MultipartFile[] files) {
        var list = new ArrayList<byte[]>();

        for (var file : files) {
            try {
                list.add(file.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return list;
    }
}
