package bsa.java.concurrency.utils.hash;

import bsa.java.concurrency.exception.HashCannotBeCalculatedException;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@Component(value = "diagonalHash")
public class DiagonalHash {
    public static final int WIDTH = 9;
    public static final int HEIGHT = 9;

    public long get(byte[] bytes) {
        try {
            var image = ImageIO.read(new ByteArrayInputStream(bytes));
            var scaledGrayImage = grayScale(image);
            return hashImage(scaledGrayImage);
        } catch (IOException e) {
            throw new HashCannotBeCalculatedException();
        }
    }

    private BufferedImage grayScale(BufferedImage image) {
        var scaledImage = image.getScaledInstance(WIDTH, HEIGHT, Image.SCALE_FAST);

        var grayedImage = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_GRAY);
        grayedImage.getGraphics().drawImage(scaledImage, 0, 0, null);

        return grayedImage;
    }

    private long hashImage(BufferedImage image) {
        long hash = 0;

        for (var i = 1; i < WIDTH; i++) {
            for (var j = 1; j < HEIGHT; j++) {
                if (brightness(image.getRGB(i, j)) > brightness(image.getRGB(i - 1, j - 1))) {
                    hash |= 1;
                }
                hash = hash << 1;
            }
        }

        return hash;
    }

    private int brightness(int value) {
        return value & 0b11111111;
    }

    public double calculateThreshold(long hash1, long hash2) {
        return 1 - (double) Long.bitCount(hash1 ^ hash2) / 64;
    }
}
