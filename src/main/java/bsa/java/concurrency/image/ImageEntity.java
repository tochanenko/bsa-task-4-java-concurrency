package bsa.java.concurrency.image;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
@Table(name = "images")
public class ImageEntity {
    @Id
    @GeneratedValue
    private long id;

    @Column(name = "uuid")
    private UUID uuid;

    @Column(name = "hash")
    private Long hash;

    @Column(name = "path")
    private String path;
}
