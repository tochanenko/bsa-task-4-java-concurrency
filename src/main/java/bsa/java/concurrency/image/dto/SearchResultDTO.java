package bsa.java.concurrency.image.dto;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class SearchResultDTO {
    private final UUID imageId;
    private final Double matchPercent;
    private final String imageUrl;
}
