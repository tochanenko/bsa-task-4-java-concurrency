package bsa.java.concurrency.image;

import bsa.java.concurrency.exception.HashCannotBeCalculatedException;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.utils.converters.MultipartFileConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@RequiredArgsConstructor
@RestController
@RequestMapping("/images")
public class ImageController {

    private final ImageService imageService;

    private final MultipartFileConverter fileConverter;

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        imageService.batchUploadImages(
                fileConverter.fileArrayToBytes(files),
                ServletUriComponentsBuilder.fromCurrentContextPath().build().toUri()
        ).join();
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file, @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) throws IOException, ExecutionException, InterruptedException {
        return imageService.searchMatches(file.getBytes(), threshold, ServletUriComponentsBuilder.fromCurrentContextPath().build().toUri()).get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) {
        imageService.deleteByUUID(imageId).join();
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages() {
        imageService.deleteAll().join();
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public void badRequest() {
        // TODO
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(HashCannotBeCalculatedException.class)
    public void hashServerError() {
        // TODO
    }

    @ResponseStatus(value = HttpStatus.INSUFFICIENT_STORAGE)
    @ExceptionHandler(IOException.class)
    public void insufficientStorage() {
        // TODO
    }
}
