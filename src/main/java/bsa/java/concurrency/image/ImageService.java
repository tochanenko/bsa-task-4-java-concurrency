package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import bsa.java.concurrency.utils.hash.DiagonalHash;
import bsa.java.concurrency.utils.mappers.ImageMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

@Service
@RequiredArgsConstructor
@Transactional
public class ImageService {
    private static final Logger logger = LoggerFactory.getLogger(ImageService.class);

    private final ImageRepository imageRepository;

    private final ImageMapper imageMapper;

    @Qualifier("diagonalHash")
    private final DiagonalHash diagonalHash;

    @Qualifier("fileSystem")
    private final FileSystem fileSystem;

    @Async
    public CompletableFuture<Void> batchUploadImages(List<byte[]> images, URI uri) {
        System.out.println("!!!!!!!!!!!!!! " + images.size() + " !!!!!!!!!!!!!!");
        for (var image : images) {
            var uuid = UUID.randomUUID();
            var path = new AtomicReference<String>();
            var hash = new AtomicLong();

            CompletableFuture.allOf(
                    CompletableFuture.supplyAsync(() -> diagonalHash.get(image)).thenAccept(hash::set),
                    fileSystem.saveFile(uuid.toString(), image, uri).thenAccept(path::set))
                    .thenAccept(value -> imageRepository.save(
                            ImageEntity.builder()
                                    .uuid(uuid)
                                    .hash(hash.get())
                                    .path(path.get()).build()
                    ));
        }

        return CompletableFuture.completedFuture(null);
    }

    @Async
    public CompletableFuture<List<SearchResultDTO>> searchMatches(byte[] file, double threshold, URI uri) {
        if (threshold <= 0.0 || threshold >= 1.0)
            throw new IllegalArgumentException("Threshold must be [0.0; 1.0]");

        AtomicLong hash = new AtomicLong();

        return CompletableFuture.supplyAsync(() -> diagonalHash.get(file)).thenAccept(hash::set)
                .thenApply(value -> imageRepository.findByHashUsingThreshold(hash.get(), threshold))
                .thenApply(result -> {
                    if (result.isEmpty()) {
                        try {
                            return batchUploadImages(List.of(file), uri).thenApply((value) -> new ArrayList<SearchResultDTO>()).get();
                        } catch (Exception e) {
                            logger.error(e.getMessage());
                        }
                    }

                    return imageMapper.entityListToDtoList(result, hash.get());
                });
    }

    @Async
    public CompletableFuture<Void> deleteByUUID(UUID uuid) {

        imageRepository.deleteByUuid(uuid);

        return fileSystem.deleteByPath(uuid.toString());
    }

    @Async
    public CompletableFuture<Void> deleteAll() {
        return CompletableFuture.allOf(CompletableFuture.runAsync(
                imageRepository::deleteAll),
                fileSystem.deleteAll()
        );
    }
}
