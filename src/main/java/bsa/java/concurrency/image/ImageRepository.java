package bsa.java.concurrency.image;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<ImageEntity, Long> {

    @Modifying
    void deleteByUuid(UUID uuid);

    @Query(
            value = "select * from images where 1.0 - count_bits(cast((images.hash # :hash) as bit(64))) / 64.0 > :threshold",
            nativeQuery = true
    )
    List<ImageEntity> findByHashUsingThreshold(long hash, double threshold);
}
