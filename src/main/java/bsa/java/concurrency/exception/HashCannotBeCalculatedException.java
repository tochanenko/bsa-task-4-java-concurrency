package bsa.java.concurrency.exception;

public class HashCannotBeCalculatedException extends RuntimeException {
    public HashCannotBeCalculatedException() {
        super("Hash cannot be calculated");
    }
}
